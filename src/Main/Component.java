package Main;

public class Component {
	private String name;
	private int id;

	public Component(int id, String name) {
		this.name = name;
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void addComponent(Component component) {
		// System.out.println("component class add");
		
	}

	public void removeComponent(Component component) {
	}

	public void printComponent() {
		
		
	}

	public void setName(String name) {
		this.name = name;
	}

	

}
