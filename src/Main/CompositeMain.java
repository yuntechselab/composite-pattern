package Main;

import java.util.Iterator;

public class CompositeMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Component leaf1 = new Leaf(1,"leaf1");
		Component leaf2 = new Leaf(2,"leaf2");
		Component composite1 = new Composite(1,"composite1");
		Component composite2 = new Composite(2,"composite2");
		composite1.addComponent(leaf1);
		composite1.addComponent(leaf2);
		composite1.addComponent(composite2);

		composite1.printComponent();
		
		
//		remove(composite1);
		// String stringValue = "�A�n";
		// int intValue = 123;
	}

//	public static void remove(Leaf leaf) {
//		System.out.println(leaf);
//	}

	public static void remove(Component component) {
		System.out.println(component);
	}
}
