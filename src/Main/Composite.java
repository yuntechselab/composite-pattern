package Main;

import java.util.ArrayList;

public class Composite extends Component{
	public Composite(int id, String name) {
		super(id, name);
		// TODO Auto-generated constructor stub
	}
	ArrayList<Component> array = new ArrayList<Component>();
	Component componentAttr1;
	Component componentAttr2;

	public void addComponent(Component component){
		array.add(component);
	}	
	public void removeComponent(Component component){}
	public void printComponent(){
		System.out.println("Composite:\t"+this.getName());
		for(Component component:array){
			component.printComponent();
		}
	}
}
